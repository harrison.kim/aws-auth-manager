#!/usr/bin/python3
import argparse
from os import path
import subprocess

def print_block(resource, contents):
    print(f"\n### {resource} ###\n")
    print(contents)
    print(f"### end {resource} ###\n")

parser = argparse.ArgumentParser(description="Manage your aws-auth config map!")
parser.add_argument("filename", help="The patch filename to use")
parser.add_argument("--debug", action='store_true', help="If set, don't actually apply the patch")

args = parser.parse_args()

if not path.exists(args.filename):
    parser.error(f"File \"{args.filename}\" does not exist!")

print(f'Using filename "{args.filename}"!')

patch_config = open(args.filename, "r").read()
print_block('patch config', patch_config)

patch_command = f"kubectl patch cm aws-auth -n kube-system --patch \"{patch_config}\""
print_block('patch command', patch_command)

if args.debug:
    print("Not running command due to debug being set!")
else:
    print("Running the patch command!")
    # subprocess.call(patch_command, shell=True)
